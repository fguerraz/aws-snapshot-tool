#!/usr/bin/python
#
# (c) 2014 blinkx - Francois Guerraz

import boto.ec2
from datetime import time,datetime

conn = boto.connect_ec2()

hour_tag=datetime.time(datetime.now()).strftime("%H00")

to_stop=conn.get_only_instances(filters={ 'tag-key' : 'StopAt', 'tag-value' : hour_tag , 'instance-state-name' : 'running'})
to_start=conn.get_only_instances(filters={ 'tag-key' : 'StartAt', 'tag-value' : hour_tag , 'instance-state-name' : 'stopped'})

if to_stop:
	conn.stop_instances([instance.id for instance in to_stop])
if to_start:
	conn.start_instances([instance.id for instance in to_start])

